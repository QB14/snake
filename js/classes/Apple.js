class Apple {
    //Propriétés:
    constructor(domZone, posX = 0, posY = 0){
    this.width = 1;
    this.height = 1;
    this.domZone = domZone;
    this.position = {x:posX,y:posY};
    this.domObject = document.createElementNS('http://www.w3.org/2000/svg','rect');
    this.colorApple = 'red';
    }
    //méthodes
    draw(){
      // on veut dessiner une pomme
        this.domZone.appendChild(this.domObject);
        this.domObject.style.width = `${this.width}px`;
        this.domObject.style.height = `${this.height}px`;
        this.domObject.style.fill = this.colorApple;
        this.domObject.style.x = `${this.position.x}px`;
        this.domObject.style.y = `${this.position.y}px`;
        this.domObject.style.rx = 1+'px';
        this.domObject.style.ry = 1+'px';

    }
  }