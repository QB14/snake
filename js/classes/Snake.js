"use strict";
class Snake {
    //Propriétés:
    constructor(domZone, posX = 0, posY = 0) {
        this.width = 1;
        this.height = 1;
        this.position = { x: posX, y: posY };
        this.direction = { x: 1, y: 0 };
        this.snakeColor = "black";
        this.domObject = document.createElementNS("http://www.w3.org/2000/svg", "rect");
        this.domZone = domZone;
        this.eventKey = "";
        this.tail = new Tail();
    }

    //méthodes
    draw() {
        // on veut dessiner le snake
        // on attribue les propriétés à l'objet du Dom et on le dessine dans la domZone (le svg)
        this.domZone.appendChild(this.domObject);
        this.domObject.style.fill = this.snakeColor;
        this.domObject.style.x = `${this.position.x}px`;
        this.domObject.style.y = `${this.position.y}px`;
        this.domObject.style.width = `${this.width}px`;
        this.domObject.style.height = `${this.height}px`;
    }

    move(keyboard) {
        this.followTail();
        if (this.direction.x == 0) {
            if (keyboard == "ArrowRight") {
                this.direction.x = 1;
                this.direction.y = 0;
            }
            if (keyboard == "ArrowLeft") {
                this.direction.x = -1;
                this.direction.y = 0;
            }
        }

        if (this.direction.y == 0) {
            if (keyboard == "ArrowUp") {
                this.direction.x = 0;
                this.direction.y = -1;
            }
            if (keyboard == "ArrowDown") {
                this.direction.x = 0;
                this.direction.y = 1;
            }
        }

        this.position.y += 1 * this.direction.y;
        this.position.x += 1 * this.direction.x;
        this.domObject.style.x = `${this.position.x}px`;
        this.domObject.style.y = `${this.position.y}px`;

    }

    followTail() {
        for (let i = this.tail.collectionTailParts.length - 1; i > 0; i--) {
            this.tail.collectionTailParts[i].position.x =  this.tail.collectionTailParts[i-1].position.x;
            this.tail.collectionTailParts[i].position.y =  this.tail.collectionTailParts[i-1].position.y;
            this.tail.collectionTailParts[i].domObject.style.x = this.tail.collectionTailParts[i - 1].domObject.style.x;
            this.tail.collectionTailParts[i].domObject.style.y = this.tail.collectionTailParts[i - 1].domObject.style.y;
        }
        if(this.tail.collectionTailParts.length!= 0){
            this.tail.collectionTailParts[0].position.x = this.position.x;
            this.tail.collectionTailParts[0].position.y = this.position.y;
            this.tail.collectionTailParts[0].domObject.style.x = this.domObject.style.x;
            this.tail.collectionTailParts[0].domObject.style.y = this.domObject.style.y;
        }

    }
}
