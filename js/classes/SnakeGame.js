class SnakeGame {
    constructor() {
        this.event = "";
        this.intervalId = null;
        this.actualKey = "";
        this.snake = "";
        this.speed = {
            snake: 5,
            difficult: {
                easy: 5,
                medium: 7,
                hard: 9,
                selected: 5,
            },
        };
        this.apple = "";
        this.activeApple = 1;
        this.pauseGame = 1;
        this.modeGame = 0;
        this.speedConditionUp = [10, 20, 30, 40, 50, 60, 70];
        this.score = "";
    }

    start(e) {
        this.event = e;
        if ((this.event.key == "Enter" && document.querySelector("#startMenu").classList.contains("hidden")) || e.target.classList.contains("startGame")) {
            if (this.pauseGame == 1) {
                this.pauseGame = 0;
            } else {
                this.pauseGame = 1;
            }
        }
        if (this.snake == "") {
            this.configGame(e);
        }

        if (this.intervalId == null && this.pauseGame == 0) {
            this.intervalId = setInterval(this.play.bind(this), 1000 / this.speed.snake);
        }

        if (this.pauseGame == 1) {
            clearInterval(this.intervalId);
            this.intervalId = null;
        }
    }

    play() {
        //on place les éléments du jeu
        this.initGame();
        // Condition lorsque le snake mange une pomme
        this.snakeEatApple();
        // Condition pour que le snake bouge
        this.snakeConditionMove();
        //Récupère et affiche les informations liées aux scores
        this.displayInfoScore();
        // augmente la vitesse du snake en fonction du nombre de pomme mangée
        this.speedUpSnake();
        if (this.snake != "") {
            //Avec ou sans le mur
            this.modeGameBorder();
        }
        if (this.snake != "") {
            //game over si le serpent mange sa queue (dégueulasse)
            this.snakeEatItself();
        }
    }

    initGame() {
        if (this.snake == "") {
            this.apple = new Apple(domZone, getRandomIntInclusive(0, domZone.viewBox.animVal.width - 1), getRandomIntInclusive(0, domZone.viewBox.animVal.height - 1));
            this.apple.draw();
            this.snake = new Snake(domZone, 10, 10);
            this.snake.draw();
        }
    }

    resetGame() {
        clearInterval(this.intervalId);
        this.snake.domObject.remove();
        this.snake.tail.collectionTailParts.forEach(tailParts => {
            tailParts.domObject.remove();
        });
        this.apple.domObject.remove();
        this.event = "";
        this.intervalId = null;
        this.actualKey = "";
        this.snake = "";
        this.speed = {
            snake: 5,
            difficult: {
                easy: 5,
                medium: 7,
                hard: 9,
                selected: this.speed.difficult.selected,
            },
        };
        this.apple = "";
        this.activeApple = 1;
        this.pauseGame = 1;
        this.modeGame = this.modeGame;
        this.speedConditionUp = [10, 20, 30, 40, 50, 60, 70];
        localStorage.setItem("score", this.score);
        document.querySelector("#previousScore").innerText = localStorage.getItem("score");
        this.score = "";
        document.querySelector("#startMenu").classList.remove("hidden");
    }

    snakeEatApple() {
        if (this.activeApple == 1) {
            if (this.snake.position.x == this.apple.position.x && this.snake.position.y == this.apple.position.y) {
                this.activeApple = 0;
                this.snake.tail.tailGrow();
                this.snake.tail.collectionTailParts[this.snake.tail.collectionTailParts.length - 1].draw();
                this.apple.position.x = getRandomIntInclusive(0, domZone.viewBox.animVal.width - 1);
                this.apple.position.y = getRandomIntInclusive(0, domZone.viewBox.animVal.height - 1);
                this.snake.tail.collectionTailParts.forEach(tailpart => {
                    while ((this.apple.position.x == this.snake.position.x && this.apple.position.y == this.snake.position.y) || (this.apple.position.x == tailpart.position.x && this.apple.position.y == tailpart.position.y)) {
                        this.apple.position.x = getRandomIntInclusive(0, domZone.viewBox.animVal.width - 1);
                        this.apple.position.y = getRandomIntInclusive(0, domZone.viewBox.animVal.height - 1);
                    }
                });
            }
        } else {
            this.activeApple = 1;
            this.apple.domObject.style.x = `${this.apple.position.x}px`;
            this.apple.domObject.style.y = `${this.apple.position.y}px`;
        }
    }

    snakeConditionMove() {
        if (this.event.key == "ArrowRight" || this.event.key == "ArrowLeft" || this.event.key == "ArrowUp" || this.event.key == "ArrowDown") {
            this.actualKey = this.event.key;
            this.snake.move(this.actualKey);
        } else {
            this.snake.move(this.actualKey);
        }
    }

    modeGameBorder() {
        if (this.modeGame == 0) {
            if (this.snake.position.x >= this.snake.domZone.viewBox.animVal.width) {
                this.snake.position.x = 0;
                this.snake.domObject.style.x = `0px`;
            }
            if (this.snake.position.x < this.snake.domZone.viewBox.animVal.x) {
                this.snake.position.x = this.snake.domZone.viewBox.animVal.width - 1;
                this.snake.domObject.style.x = `${this.snake.position.x}px`;
            }
            if (this.snake.position.y >= this.snake.domZone.viewBox.animVal.height) {
                this.snake.position.y = 0;
                this.snake.domObject.style.y = `0px`;
            }
            if (this.snake.position.y < this.snake.domZone.viewBox.animVal.y) {
                this.snake.position.y = this.snake.domZone.viewBox.animVal.height - 1;
                this.snake.domObject.style.y = `${this.snake.position.y}px`;
            }
        } else if (this.modeGame == 1) {
            if (this.snake.position.x >= this.snake.domZone.viewBox.animVal.width) {
                this.resetGame();
            } else if (this.snake.position.x < this.snake.domZone.viewBox.animVal.x) {
                this.resetGame();
            } else if (this.snake.position.y >= this.snake.domZone.viewBox.animVal.height) {
                this.resetGame();
            } else if (this.snake.position.y < this.snake.domZone.viewBox.animVal.y) {
                this.resetGame();
            }
        }
    }

    snakeEatItself() {
        this.snake.tail.collectionTailParts.forEach(tailParts => {
            if (this.snake != "") {
                if (this.snake.domObject.style.x == tailParts.domObject.style.x && this.snake.domObject.style.y == tailParts.domObject.style.y) {
                    this.resetGame();
                }
            }
        });
    }

    displayInfoScore() {
        this.score = this.snake.tail.collectionTailParts.length * 10;
        document.querySelector("#score").innerText = this.score;
        this.scoreApple = this.snake.tail.collectionTailParts.length;
        document.querySelector("#scoreApple").innerText = this.scoreApple;

        document.querySelector("#speed").innerText = this.speed.snake;
    }

    speedUpSnake() {
        if (this.speedConditionUp.includes(this.scoreApple)) {
            this.speedConditionUp.splice(this.speedConditionUp.indexOf(this.scoreApple), 1);
            this.speed.snake += 2;
            clearInterval(this.intervalId);
            this.intervalId = setInterval(this.play.bind(this), 1000 / this.speed.snake);
        }
    }

    configGame(e) {
        if (isNaN(parseInt(e.target.dataset.border, 10))) {
        } else {
            this.modeGame = e.target.dataset.border;
        }

        if (e.target.dataset.difficult == 0) {
            this.speed.difficult.selected = this.speed.difficult.easy;
        } else if (e.target.dataset.difficult == 1) {
            this.speed.difficult.selected = this.speed.difficult.medium;
        } else if (e.target.dataset.difficult == 2) {
            this.speed.difficult.selected = this.speed.difficult.hard;
        }
        this.speed.snake = this.speed.difficult.selected;
    }
}
