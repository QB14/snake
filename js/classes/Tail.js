class Tail {
    constructor(){
        this.collectionTailParts = [];
        this.domZone = domZone;
    }

    tailGrow(){
        this.collectionTailParts.push(new TailPart(this.domZone));
    }
}