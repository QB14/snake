class TailPart{
    //Propriétés:
    constructor(domZone, posX = 0, posY = 0) {
        this.width = 1;
        this.height = 1;
        this.position = { x: posX, y: posY };
        this.direction = { x: 0, y: 0 };
        this.snakeColor = "green";
        this.domObject = document.createElementNS(
            "http://www.w3.org/2000/svg",
            "rect"
        );
        this.domZone = domZone;
        this.eventKey = "";
    }

    //méthodes
    draw() {
        // on veut dessiner le snake
        // on attribue les propriétés à l'objet du Dom et on le dessine dans la domZone (le svg)
        this.domZone.appendChild(this.domObject);
        this.domObject.style.fill = this.snakeColor;
        this.domObject.style.x = `${this.position.x}px`;
        this.domObject.style.y = `${this.position.y}px`;
        this.domObject.style.width = `${this.width}px`;
        this.domObject.style.height = `${this.height}px`;
    }
}
