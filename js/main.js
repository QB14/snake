'use strict'
//variable

/**
 * @var {Element} domZone Sélection du DOM Pour les éléments snake
 */
let domZone;

/**
//  * @var  {Array} snakes tableau dans lequel sont pushés la collection de snake.
// */
// const snakes = [];

const snakeGame = new SnakeGame();

let buttonGameModes;

let buttonGameDifficulties;




function interactionSnake(e){
    snakeGame.start(e);
}

function userConfigGame(e){
    snakeGame.start(e);
    if(this.classList.contains('startGame')){
        document.querySelector('#startMenu').classList.add("hidden");
    }

}

function clickOnButton(){
        buttonGameModes.forEach((buttonGameModes)=>{
        buttonGameModes.classList.remove("active");
    })
    this.classList.add("active");
        
}
function clickOnButtonDifficulties(){
    buttonGameDifficulties.forEach((buttonGameDifficultie)=>{
    buttonGameDifficultie.classList.remove("active");
});
    this.classList.add("active");
}



// on veut charger le DOM
document.addEventListener('DOMContentLoaded',()=>{
    document.querySelector("#previousScore").innerText = localStorage.getItem('score');
    domZone = document.querySelector('#domZone');
    document.addEventListener('keydown',interactionSnake);

    document.querySelectorAll('.selectBorder').forEach((modeBorder)=>{
        modeBorder.addEventListener('click', userConfigGame);
    });
    document.querySelectorAll('.difficults').forEach((difficult)=>{
        difficult.addEventListener('click', userConfigGame);
    });

    buttonGameModes = document.querySelectorAll('#startMenu .selectBorder button'); 
    buttonGameModes.forEach((buttonGameMode)=>{
        buttonGameMode.addEventListener('click',clickOnButton);
    });

    buttonGameDifficulties = document.querySelectorAll('#startMenu .difficults button');
    buttonGameDifficulties.forEach((buttonGameDifficultie)=>{
        buttonGameDifficultie.addEventListener('click',clickOnButtonDifficulties);
    })


    document.querySelector('.startGame').addEventListener('click', userConfigGame);

});
