/** Return un nombre compris entre min et max inclus
 * 
 * @param {Number} min 
 * @param {Number} max 
 * @return {Number} un nombre alétoire
 */
function getRandomIntInclusive(min, max) {
    // Converti min en entier (supérieur ou egal) si ce n'est pas déjà la cas
    min = Math.ceil(min);
    // Converti max en entier (inférieur ou egal)
    max = Math.floor(max);
    // Renvoi un nombre compris entre min et max inclu
    return Math.floor(Math.random() * (max - min + 1)) + min;
}